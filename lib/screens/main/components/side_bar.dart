import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/screens/main/components/header.dart';
import 'package:agungbudip_my_id/screens/main/components/menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SideBar extends StatelessWidget {
  const SideBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(gradient: kGradientColor),
      child: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(kDefaultPadding),
              child: Header(),
            ),
            SizedBox(height: kDefaultPadding),
            Menu(),
          ],
        ),
      ),
    );
  }
}
