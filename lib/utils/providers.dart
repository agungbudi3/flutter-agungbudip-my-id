import 'package:agungbudip_my_id/controllers/journey/journey_bloc.dart';
import 'package:agungbudip_my_id/controllers/menu/menu_cubit.dart';
import 'package:agungbudip_my_id/controllers/portfolio/portfolio_bloc.dart';
import 'package:agungbudip_my_id/controllers/snackbar/snackbar_cubit.dart';
import 'package:agungbudip_my_id/controllers/toolbar/toolbar_cubit.dart';
import 'package:agungbudip_my_id/utils/network/endpoints.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

List<BlocProvider> bloc = [
  BlocProvider<MenuCubit>(create: (c) => MenuCubit()),
  BlocProvider<ToolbarCubit>(create: (c) => ToolbarCubit()),
  BlocProvider<SnackbarCubit>(create: (c) => SnackbarCubit()),
  BlocProvider<JourneyBloc>(
    create: (c) => JourneyBloc(
      RepositoryProvider.of<Endpoints>(c),
    )..add(JourneyOnRefresh()),
  ),
  BlocProvider<PortfolioBloc>(
    create: (c) => PortfolioBloc(
      RepositoryProvider.of<Endpoints>(c),
    )..add(PortfolioOnRefresh()),
  ),
];

List<RepositoryProvider> repo = [
  RepositoryProvider<Endpoints>(create: (c) => Endpoints()),
];
