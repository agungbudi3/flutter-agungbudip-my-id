import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MenuItem extends StatefulWidget {
  const MenuItem({
    Key key,
    @required this.text,
    @required this.isActive,
    @required this.press,
  }) : super(key: key);

  final String text;
  final bool isActive;
  final VoidCallback press;

  @override
  _MenuItemState createState() => _MenuItemState();
}

class _MenuItemState extends State<MenuItem> {
  bool _isHover = false;

  Color _bgColor() {
    if (widget.isActive) {
      return Colors.white.withOpacity(0.2);
    } else if (!widget.isActive && _isHover) {
      return Colors.white.withOpacity(0.1);
    }
    return Colors.transparent;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onHover: (value) {
        setState(() {
          _isHover = value;
        });
      },
      onTap: widget.press,
      child: Container(
        width: double.infinity,
        color: _bgColor(),
        padding: EdgeInsets.symmetric(
          vertical: kDefaultPadding,
        ),
        child: Row(
          children: [
            SizedBox(width: kDefaultPadding),
            Text(
              widget.text,
              textAlign: TextAlign.start,
              style: widget.isActive
                  ? kStyleRobotoNormal14
                  : kStyleRobotoNormal14.apply(
                      color: Colors.white.withOpacity(0.5),
                    ),
            ),
            if (widget.isActive || _isHover) ...[
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Image.asset(
                  "assets/icons/chevron-right.png",
                  color:
                      _isHover ? Colors.white.withOpacity(0.5) : Colors.white,
                  width: 7,
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
