import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/cupertino.dart';

class JourneyRadio extends StatelessWidget {
  const JourneyRadio({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 20,
      height: 20,
      child: Container(
        width: 10,
        height: 10,
        margin: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: kPrimaryColor,
        ),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Color(0xffEAE7E7),
      ),
    );
  }
}