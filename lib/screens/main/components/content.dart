import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/controllers/menu/menu_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Content extends StatelessWidget {
  const Content({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: kDefaultPadding + MediaQuery.of(context).padding.left),
      child: Builder(
        builder: (c) {
          var state = c.watch<MenuCubit>().state;
          return MenuCubit.getMenuItemWidget(state);
        },
      ),
    );
  }
}
