import 'package:agungbudip_my_id/controllers/portfolio/portfolio_bloc.dart';
import 'package:agungbudip_my_id/screens/portfolio/components/portfolio_header.dart';
import 'package:agungbudip_my_id/screens/portfolio/components/portfolio_item.dart';
import 'package:agungbudip_my_id/screens/portfolio/components/portfolio_loading.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Portfolio extends StatefulWidget {
  const Portfolio({Key key}) : super(key: key);

  @override
  _PortfolioState createState() => _PortfolioState();
}

class _PortfolioState extends State<Portfolio> {
  ScrollController _scrollController;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset >=
              _scrollController.position.maxScrollExtent &&
          !_scrollController.position.outOfRange) {
        if (!isLoading && context.read<PortfolioBloc>().hasNext) {
          context.read<PortfolioBloc>().add(PortfolioOnLoad());
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Title(
      color: FlavorConfig.instance.primaryColor,
      title: "Portfolio - ${FlavorConfig.instance.name}",
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverAppBar(
                backgroundColor: Colors.transparent,
                pinned: true,
                toolbarHeight: 0,
              ),
            ),
          ];
        },
        body: Builder(
          builder: (c) {
            return RefreshIndicator(
              onRefresh: () async {
                c.read<PortfolioBloc>().add(PortfolioOnRefresh());
              },
              child: Builder(
                builder: (context) {
                  var state = context.watch<PortfolioBloc>().state;
                  var items = context.read<PortfolioBloc>().items;
                  return CustomScrollView(
                    controller: _scrollController,
                    physics: AlwaysScrollableScrollPhysics(),
                    slivers: [
                      SliverOverlapInjector(
                        handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                          context,
                        ),
                      ),
                      SliverToBoxAdapter(
                        child: PortfolioHeader(),
                      ),
                      SliverToBoxAdapter(
                        child: SizedBox(height: kDefaultPadding * 2),
                      ),
                      if (state is PortfolioRefresh) ...[
                        SliverToBoxAdapter(
                          child: PortfolioLoading(),
                        ),
                      ],
                      SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            var item = items[index];
                            return PortfolioItem(
                              appName: item.name,
                              description: item.description,
                              images: item.images,
                              tags: item.tags,
                            );
                          },
                          childCount: items.length,
                        ),
                      ),
                      if (state is PortfolioLoad) ...[
                        SliverToBoxAdapter(
                          child: PortfolioLoading(),
                        ),
                      ],
                    ],
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
