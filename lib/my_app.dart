import 'package:agungbudip_my_id/utils/providers.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/screens/main/main_screen.dart';
import 'package:agungbudip_my_id/utils/network/endpoints.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: repo,
      child: MultiBlocProvider(
        providers: bloc,
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: FlavorConfig.instance.name,
          theme: ThemeData(
            primarySwatch: FlavorConfig.instance.primarySwatch,
            primaryColor: FlavorConfig.instance.primaryColor,
            scaffoldBackgroundColor: kBgColor,
          ),
          home: Container(
            decoration: BoxDecoration(
              color: kBgColor,
              image: DecorationImage(
                image: AssetImage('assets/images/tile.png'),
                repeat: ImageRepeat.repeat,
                scale: 3.5,
              ),
            ),
            child: MainScreen(),
          ),
        ),
      ),
    );
  }
}
