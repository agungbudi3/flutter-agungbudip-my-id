import 'package:flutter/material.dart';

class UnderContraction extends StatelessWidget {
  const UnderContraction({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        elevation: 5,
        child: Container(
          width: 400,
          height: 300,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
            image: DecorationImage(
              image: AssetImage("assets/images/under_construction.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}