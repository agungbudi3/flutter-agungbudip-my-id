part of 'snackbar_cubit.dart';

@immutable
abstract class SnackbarState {}

class SnackbarInitial extends SnackbarState {}
class SnackbarMessage extends SnackbarState {
  final String message;

  SnackbarMessage(this.message);
}
