import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactMe extends StatelessWidget {
  const ContactMe({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Title(
      color: FlavorConfig.instance.primaryColor,
      title: "Contact Me - ${FlavorConfig.instance.name}",
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: kDefaultPadding),
                Text(
                  "CONTACT ME",
                  style: kStyleRobotoBold36.apply(
                    color: kPrimaryColor,
                  ),
                ),
                SizedBox(height: kDefaultPadding / 2),
                Text(
                  "I am available for hire and open to any ideas of cooperation.",
                  style: kStyleRobotoNormal14.apply(
                    color: kGreyColor1,
                  ),
                ),
                SizedBox(height: kDefaultPadding * 2),
                ContactItem(
                  title: "Email",
                  icon: "assets/icons/gmail.png",
                  account: "agungbudi3@gmail.com",
                  press: () async {
                    await launch(
                      Uri(
                        scheme: 'mailto',
                        path: 'agungbudi3@gmail.com',
                      ).toString(),
                    );
                  },
                ),
                ContactItem(
                  title: "LikedIn",
                  icon: "assets/icons/linkedin.png",
                  account: "Agung Budi",
                  press: () async {
                    await launch("https://www.linkedin.com/in/agung-budi-520451178/");
                  },
                ),
                ContactItem(
                  title: "Github",
                  icon: "assets/icons/github.png",
                  account: "agungbudip",
                  press: () async {
                    await launch("https://github.com/agungbudip");
                  },
                ),
                ContactItem(
                  title: "Gitlab",
                  icon: "assets/icons/gitlab.png",
                  account: "agungbudi3",
                  press: () async {
                    await launch("https://gitlab.com/agungbudi3");
                  },
                ),
                ContactItem(
                  title: "Twitter",
                  icon: "assets/icons/twitter.png",
                  account: "agungbudip",
                  press: () async {
                    await launch("https://twitter.com/agungbudip");
                  },
                ),
                SizedBox(height: kDefaultPadding * 2),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ContactItem extends StatelessWidget {
  const ContactItem({
    Key key,
    @required this.title,
    @required this.icon,
    @required this.account,
    @required this.press,
  }) : super(key: key);

  final String title;
  final String icon;
  final String account;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: InkWell(
        onTap: press,
        child: Row(
          children: [
            Container(
              width: 100,
              child: Text(
                title,
                style: kStyleRobotoNormal15.apply(
                  color: kGreyColor1,
                ),
              ),
            ),
            Container(
              width: 24,
              height: 24,
              padding: EdgeInsets.only(right: 5),
              child: Center(
                child: Image.asset(
                  icon,
                  width: 12,
                ),
              ),
            ),
            Text(
              account,
              style: kStyleRobotoNormal15.apply(
                color: kPrimaryColor,
                decoration: TextDecoration.underline,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
