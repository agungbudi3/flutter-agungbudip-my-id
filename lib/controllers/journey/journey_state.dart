part of 'journey_bloc.dart';

@immutable
abstract class JourneyState {}

class JourneyInitial extends JourneyState {}

class JourneyRefresh extends JourneyState {}

class JourneyRefreshSuccess extends JourneyState {}

class JourneyRefreshError extends JourneyState {
  final String message;

  JourneyRefreshError(this.message);
}

class JourneyLoad extends JourneyState {}

class JourneyLoadSuccess extends JourneyState {}

class JourneyLoadError extends JourneyState {
  final String message;

  JourneyLoadError(this.message);
}
