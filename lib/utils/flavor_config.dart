import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';

enum Flavor { DEV, PROD }

class FlavorValues {
  FlavorValues({
    @required this.baseUrl,
  });

  final String baseUrl;
}

class FlavorConfig {
  final Flavor flavor;
  final String name;
  final MaterialColor primarySwatch;
  final Color primaryColor;
  final Color accentColor;
  final FlavorValues values;
  static FlavorConfig _instance;

  factory FlavorConfig({
    @required Flavor flavor,
    MaterialColor primarySwatch: kMaterialPrimaryColor,
    Color primaryColor: kPrimaryColor,
    Color accentColor: kGreyColor1,
    @required FlavorValues values,
  }) {
    var name = "Agung Budi Prasetyo";
    if (flavor == Flavor.DEV) {
      name = "[DEV] $name";
    }
    _instance ??= FlavorConfig._internal(
      flavor,
      name,
      primarySwatch,
      primaryColor,
      accentColor,
      values,
    );
    return _instance;
  }

  FlavorConfig._internal(
    this.flavor,
    this.name,
    this.primarySwatch,
    this.primaryColor,
    this.accentColor,
    this.values,
  );

  static FlavorConfig get instance {
    return _instance;
  }

  static bool isProduction() => _instance.flavor == Flavor.PROD;

  static bool isDevelopment() => _instance.flavor == Flavor.DEV;
}
