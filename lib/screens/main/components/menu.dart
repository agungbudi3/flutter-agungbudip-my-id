import 'package:agungbudip_my_id/controllers/menu/menu_cubit.dart';
import 'package:agungbudip_my_id/screens/main/components/menu_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Menu extends StatelessWidget {
  const Menu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (c) {
      var state = c.watch<MenuCubit>().state;
      var menuItems = c.read<MenuCubit>().menuItems;
      return Column(
        children: List.generate(menuItems.length, (index) {
          var menuItem = menuItems[index];
          return MenuItem(
            text: MenuCubit.getMenuItemText(menuItem),
            isActive: menuItem == state,
            press: () {
              c.read<MenuCubit>().openMenu(menuItem);
            },
          );
        }),
      );
    });
  }
}
