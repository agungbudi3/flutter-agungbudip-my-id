import 'package:agungbudip_my_id/controllers/snackbar/snackbar_cubit.dart';
import 'package:agungbudip_my_id/controllers/toolbar/toolbar_cubit.dart';
import 'package:agungbudip_my_id/screens/main/components/content.dart';
import 'package:agungbudip_my_id/screens/main/components/side_bar.dart';
import 'package:agungbudip_my_id/screens/main/components/toolbar.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var isShow = context.watch<ToolbarCubit>().state;
    var _toolbarSize = 148.0;
    if (!isShow) {
      _toolbarSize = 48.0;
    }
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: Responsive.isDesktop(context)
          ? null
          : PreferredSize(
              preferredSize: Size.fromHeight(_toolbarSize),
              child: Toolbar(isShow: isShow),
            ),
      body: BlocListener<SnackbarCubit, SnackbarState>(
        listener: (c, state) {
          if(state is SnackbarMessage && state.message.isNotEmpty){
            final snackBar = SnackBar(content: Text(state.message));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Color(0x26000000),
                  spreadRadius: 1,
                  blurRadius: 6,
                  offset: Offset(3, 4), // changes position of shadow
                ),
              ],
            ),
            constraints: BoxConstraints(maxWidth: kMaxWidth),
            child: Row(
              children: [
                if (Responsive.isDesktop(context)) ...[
                  Expanded(
                    flex: 3,
                    child: SideBar(),
                  ),
                ],
                Expanded(
                  flex: 9,
                  child: Content(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
