class Meta {
  Pagination _pagination;

  Pagination get pagination => _pagination;

  Meta({
    Pagination pagination}){
    _pagination = pagination;
  }

  Meta.fromJson(dynamic json) {
    _pagination = json["pagination"] != null ? Pagination.fromJson(json["pagination"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_pagination != null) {
      map["pagination"] = _pagination.toJson();
    }
    return map;
  }

}

class Pagination {
  int _total;
  int _count;
  int _perPage;
  int _currentPage;
  int _totalPages;

  int get total => _total;
  int get count => _count;
  int get perPage => _perPage;
  int get currentPage => _currentPage;
  int get totalPages => _totalPages;

  Pagination({
    int total,
    int count,
    int perPage,
    int currentPage,
    int totalPages}){
    _total = total;
    _count = count;
    _perPage = perPage;
    _currentPage = currentPage;
    _totalPages = totalPages;
  }

  Pagination.fromJson(dynamic json) {
    _total = json["total"];
    _count = json["count"];
    _perPage = json["per_page"];
    _currentPage = json["current_page"];
    _totalPages = json["total_pages"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["total"] = _total;
    map["count"] = _count;
    map["per_page"] = _perPage;
    map["current_page"] = _currentPage;
    map["total_pages"] = _totalPages;
    return map;
  }

}