import 'package:agungbudip_my_id/controllers/journey/journey_bloc.dart';
import 'package:agungbudip_my_id/controllers/snackbar/snackbar_cubit.dart';
import 'package:agungbudip_my_id/screens/my_journey/components/journey_header.dart';
import 'package:agungbudip_my_id/screens/my_journey/components/journey_item.dart';
import 'package:agungbudip_my_id/screens/my_journey/components/journey_loading.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyJourney extends StatefulWidget {
  const MyJourney({Key key}) : super(key: key);

  @override
  _MyJourneyState createState() => _MyJourneyState();
}

class _MyJourneyState extends State<MyJourney> {
  ScrollController _scrollController;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.offset >=
              _scrollController.position.maxScrollExtent &&
          !_scrollController.position.outOfRange) {
        if (!isLoading && context.read<JourneyBloc>().hasNext) {
          context.read<JourneyBloc>().add(JourneyOnLoad());
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Title(
      color: FlavorConfig.instance.primaryColor,
      title: "My Journey - ${FlavorConfig.instance.name}",
      child: BlocListener<JourneyBloc, JourneyState>(
        listener: (c, s) {
          if(s is JourneyRefreshError){
            c.read<SnackbarCubit>().showSnackbar(s.message);
          }
          if(s is JourneyLoadError){
            c.read<SnackbarCubit>().showSnackbar(s.message);
          }
        },
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return [
              SliverOverlapAbsorber(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                sliver: SliverAppBar(
                  backgroundColor: Colors.transparent,
                  pinned: true,
                  toolbarHeight: 0,
                ),
              ),
            ];
          },
          body: Builder(
            builder: (c) {
              return RefreshIndicator(
                onRefresh: () async {
                  c.read<JourneyBloc>().add(JourneyOnRefresh());
                },
                child: Builder(
                  builder: (context) {
                    var state = context.watch<JourneyBloc>().state;
                    var items = context.read<JourneyBloc>().items;
                    return CustomScrollView(
                      controller: _scrollController,
                      physics: AlwaysScrollableScrollPhysics(),
                      slivers: [
                        SliverOverlapInjector(
                          handle:
                              NestedScrollView.sliverOverlapAbsorberHandleFor(
                            context,
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: JourneyHeader(),
                        ),
                        SliverToBoxAdapter(
                          child: SizedBox(height: kDefaultPadding * 2),
                        ),
                        if (state is JourneyRefresh) ...[
                          SliverToBoxAdapter(
                            child: JourneyLoading(),
                          ),
                        ],
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                              return JourneyItem(
                                year: items[index].year,
                                title: items[index].title,
                                info: items[index].info,
                                subtitle: items[index].subtitle,
                                content: items[index].story,
                              );
                            },
                            childCount: items.length,
                          ),
                        ),
                        if (state is JourneyLoad) ...[
                          SliverToBoxAdapter(
                            child: JourneyLoading(),
                          ),
                        ],
                      ],
                    );
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
