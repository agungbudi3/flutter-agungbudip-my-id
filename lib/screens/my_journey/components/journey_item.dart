import 'package:agungbudip_my_id/screens/my_journey/components/journey_radio.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class JourneyItem extends StatelessWidget {
  const JourneyItem({
    Key key,
    @required this.year,
    @required this.title,
    this.subtitle,
    @required this.info,
    @required this.content,
  }) : super(key: key);

  final String year;
  final String title;
  final String subtitle;
  final String info;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 37,
              child: Text(
                year,
                style: kStylePoppinsSemiBold14.apply(
                  color: kGreyColor2,
                ),
              ),
            ),
            SizedBox(width: kDefaultPadding),
            Expanded(
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(
                          color: kPrimaryColor,
                          width: 6,
                        ),
                      ),
                    ),
                    padding: EdgeInsets.only(
                      left: kDefaultPadding,
                      bottom: kDefaultPadding,
                    ),
                    margin: EdgeInsets.only(left: (20 / 2) - (6 / 2)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: kStylePoppinsBold14.apply(
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(height: kDefaultPadding / 4),
                        Text.rich(
                          TextSpan(
                            text: subtitle != null && subtitle.isNotEmpty
                                ? "$subtitle "
                                : "",
                            style: kStyleRobotoNormal14.apply(
                              color: Colors.black,
                            ),
                            children: [
                              TextSpan(
                                text: info,
                                style: kStyleRobotoNormal14.apply(
                                  color: kGreyColor2,
                                ),
                              ),
                            ],
                          ),
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(height: kDefaultPadding / 2),
                        MarkdownBody(
                          data: content,
                          styleSheet: MarkdownStyleSheet(
                            textAlign: WrapAlignment.spaceBetween,
                            p: kStyleRobotoNormal15.apply(
                              color: kGreyColor1,
                            ),
                            listBullet: kStyleRobotoNormal15.apply(
                              color: kGreyColor1,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  JourneyRadio(),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
