import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'snackbar_state.dart';

class SnackbarCubit extends Cubit<SnackbarState> {
  SnackbarCubit() : super(SnackbarInitial());

  showSnackbar(String message) => emit(SnackbarMessage(message));
}
