import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';

class PortfolioHeader extends StatelessWidget {
  const PortfolioHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: kDefaultPadding),
        Text(
          "PORTFOLIO",
          style: kStyleRobotoBold36.apply(
            color: kPrimaryColor,
          ),
        ),
        SizedBox(height: kDefaultPadding / 2),
        Text(
          "See my portfolios below, maybe the portfolios that I show below are all mobile apps. but i also work on some web projects there are using laravel and also react js. but I haven't been able to capture it yet. all of them are custom web app, and I rarely use cms",
          style: kStyleRobotoNormal14.apply(
            color: kGreyColor1,
          ),
          textAlign: TextAlign.start,
        ),
      ],
    );
  }
}