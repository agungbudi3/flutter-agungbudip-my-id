import 'package:agungbudip_my_id/screens/my_journey/components/journey_radio.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/responsive.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:full_screen_image/full_screen_image.dart';

class PortfolioItem extends StatelessWidget {
  const PortfolioItem({
    Key key,
    @required this.appName,
    this.description = "",
    this.images = defaultList,
    this.tags = defaultList,
  }) : super(key: key);

  static const List<String> defaultList = [];
  final String appName;
  final String description;
  final List<String> images;
  final List<String> tags;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          appName,
          style: kStylePoppinsBold14.copyWith(
            color: Colors.black,
            fontSize: 16,
          ),
        ),
        SizedBox(height: kDefaultPadding / 4),
        if (Responsive.isMobile(context)) ...[
          PortfolioImage(images: images),
          SizedBox(height: kDefaultPadding / 4),
          PortfolioDescription(description: description),
          SizedBox(height: kDefaultPadding / 4),
        ] else ...[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: PortfolioImage(images: images),
              ),
              Expanded(
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
                  child: PortfolioDescription(description: description),
                ),
              ),
            ],
          ),
        ],
        SizedBox(height: kDefaultPadding / 4),
        Container(
          height: 30,
          child: ListView.builder(
            itemCount: tags.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (ctx, idx) {
              String tag = tags[idx];
              return Container(
                padding: EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
                margin: EdgeInsets.only(right: kDefaultPadding / 2),
                color: kPrimaryColor.withOpacity(0.8),
                child: Center(
                  child: Text(
                    tag,
                    style: kStyleRobotoNormal14.apply(
                      color: Colors.white,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        SizedBox(height: kDefaultPadding),
      ],
    );
  }
}

class PortfolioDescription extends StatelessWidget {
  const PortfolioDescription({
    Key key,
    @required this.description,
  }) : super(key: key);

  final String description;

  @override
  Widget build(BuildContext context) {
    return Text(
      description,
      style: kStyleRobotoNormal15.apply(
        color: kGreyColor1,
      ),
    );
  }
}

class PortfolioImage extends StatelessWidget {
  const PortfolioImage({
    Key key,
    @required this.images,
  }) : super(key: key);

  final List<String> images;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 800 / 450,
      child: images.length == 0
          ? Container(
              color: Color(0xffdcdcdc),
            )
          : images.length == 1
              ? Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: kPrimaryColor,
                  ),
                  child: FullScreenWidget(
                    backgroundIsTransparent: true,
                    child: Center(
                      child: Hero(
                        tag: "smallImage",
                        child: Image.network(
                          images[0],
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                )
              : CarouselSlider(
                  options: CarouselOptions(
                    aspectRatio: 800 / 450,
                    autoPlayAnimationDuration: Duration(milliseconds: 1000),
                    autoPlay: true,
                    viewportFraction: 1,
                    enlargeCenterPage: false,
                  ),
                  items: images.map((image) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: kPrimaryColor,
                          ),
                          child: FullScreenWidget(
                            backgroundIsTransparent: true,
                            child: Center(
                              child: Hero(
                                tag: "smallImage",
                                child: Image.network(
                                  image,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }).toList(),
                ),
    );
  }
}
