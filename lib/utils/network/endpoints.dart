import 'package:agungbudip_my_id/models/journey.dart';
import 'package:agungbudip_my_id/models/remote/journey_res.dart';
import 'package:agungbudip_my_id/models/remote/portfolio_res.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:agungbudip_my_id/utils/network/api_result.dart';
import 'package:agungbudip_my_id/utils/network/network_exceptions.dart';
import 'package:dio/dio.dart';

class Endpoints {
  Dio _dio;

  Endpoints() {
    _dio = Dio();

    _dio.options.baseUrl = FlavorConfig.instance.values.baseUrl;

    _dio.options.connectTimeout = kConnectionTimeout;
    _dio.options.receiveTimeout = kConnectionReadTimeout;

    _dio.options.headers['content-type'] = 'application/json';
  }

  Future<ApiResult<JourneyRes>> getJourneys({
    Map<String, dynamic> params,
  }) async {
    try {
      final response = await _dio.get(
        'api/journeys',
        queryParameters: params,
      );
      return ApiResult.success(
        data: JourneyRes.fromJson(response.data),
      );
    } catch (e, s) {
      print(e);
      print(s);
      return ApiResult.failure(
        error: NetworkExceptions.getDioException(e),
      );
    }
  }

  Future<ApiResult<PortfolioRes>> getPortfolios({
    Map<String, dynamic> params,
  }) async {
    try {
      final response = await _dio.get(
        'api/portfolios',
        queryParameters: params,
      );
      return ApiResult.success(
        data: PortfolioRes.fromJson(response.data),
      );
    } catch (e, s) {
      print(e);
      print(s);
      return ApiResult.failure(
        error: NetworkExceptions.getDioException(e),
      );
    }
  }
}
