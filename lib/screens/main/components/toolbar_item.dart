import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';

class ToolbarItem extends StatelessWidget {
  const ToolbarItem({
    Key key,
    @required this.text,
    @required this.isActive,
    @required this.press,
  }) : super(key: key);

  final String text;
  final bool isActive;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Container(
        height: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
        child: Center(
          child: Text(
            text,
            textAlign: TextAlign.start,
            style: kStyleRobotoBold14NoHeight.apply(
              color: Colors.white.withOpacity(isActive ? 1 : 0.5),
            ),
          ),
        ),
      ),
    );
  }
}