part of 'journey_bloc.dart';

@immutable
abstract class JourneyEvent {}

class JourneyOnRefresh extends JourneyEvent {}
class JourneyOnLoad extends JourneyEvent {}