import 'package:agungbudip_my_id/controllers/menu/menu_cubit.dart';
import 'package:agungbudip_my_id/controllers/toolbar/toolbar_cubit.dart';
import 'package:agungbudip_my_id/screens/main/components/toolbar_item.dart';
import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Toolbar extends StatelessWidget {
  const Toolbar({
    Key key,
    this.isShow = true,
  }) : super(key: key);

  final bool isShow;

  @override
  Widget build(BuildContext context) {
    var paddingTop = MediaQuery.of(context).padding.top;
    return Column(
      children: [
        if (isShow) ...[
          Container(
            padding: EdgeInsets.only(top: paddingTop),
            constraints: BoxConstraints(maxWidth: kMaxWidth),
            decoration: BoxDecoration(gradient: kGradientColor),
            child: Wrap(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: kDefaultPadding / 2 +
                        MediaQuery.of(context).padding.left,
                    vertical: kDefaultPadding,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        child: Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/images/avatar.jpg"),
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Colors.white,
                            width: kDefaultPadding / 10,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: kDefaultPadding,
                      ),
                      Expanded(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FittedBox(
                                fit: BoxFit.fitWidth,
                                child: Text.rich(
                                  TextSpan(
                                    text: "AGUNG",
                                    style: kStyleRobotoBold24NoHeight,
                                    children: [
                                      TextSpan(
                                        text: " BUDI",
                                        style:
                                            kStyleRobotoNormal24NoHeight.apply(
                                          color: Colors.white.withOpacity(0.5),
                                        ),
                                      ),
                                      TextSpan(
                                        text: " PRASETYO",
                                        style: kStyleRobotoBold24NoHeight,
                                      )
                                    ],
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              FittedBox(
                                fit: BoxFit.fitWidth,
                                child: Text(
                                  "SOFTWARE ENGINEER | MOBILE APP DEVELOPER",
                                  style: kStyleRobotoNormal12NoHeight.apply(
                                    color: Colors.white.withOpacity(0.5),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
        Container(
          height: 48,
          padding: EdgeInsets.only(top: isShow ? 0 : paddingTop),
          color: kMaterialPrimaryColor[50],
          child: Row(
            children: [
              Expanded(
                child: Builder(builder: (c) {
                  var state = c.watch<MenuCubit>().state;
                  var menuItems = c.read<MenuCubit>().menuItems;
                  return ListView.builder(
                    padding: EdgeInsets.symmetric(
                        horizontal: kDefaultPadding / 2 +
                            MediaQuery.of(context).padding.left),
                    scrollDirection: Axis.horizontal,
                    itemCount: menuItems.length,
                    itemBuilder: (context, index) {
                      var menuItem = menuItems[index];
                      return ToolbarItem(
                        text: MenuCubit.getMenuItemText(menuItem),
                        isActive: menuItem == state,
                        press: () {
                          c.read<MenuCubit>().openMenu(menuItem);
                        },
                      );
                    },
                  );
                }),
              ),
              Container(
                width: 48,
                height: 48,
                child: IconButton(
                  icon: Image.asset(
                    isShow
                        ? "assets/icons/chevron-up.png"
                        : "assets/icons/chevron-down.png",
                    color: Colors.white,
                    width: 12,
                  ),
                  onPressed: () {
                    context.read<ToolbarCubit>().hideShowToolbar();
                  },
                  iconSize: 12,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
