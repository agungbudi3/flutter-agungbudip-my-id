import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

class ToolbarCubit extends Cubit<bool> {
  ToolbarCubit() : super(true);

  hideShowToolbar() => emit(!state);
}
