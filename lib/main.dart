import 'package:agungbudip_my_id/my_app.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FlavorConfig(
    flavor: Flavor.PROD,
    values: FlavorValues(
      baseUrl: "https://agungbudip.my.id/",
    ),
  );
  runApp(MyApp());
}
