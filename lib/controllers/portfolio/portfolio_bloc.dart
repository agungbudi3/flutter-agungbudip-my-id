import 'dart:async';

import 'package:agungbudip_my_id/models/portfolio.dart';
import 'package:agungbudip_my_id/utils/network/endpoints.dart';
import 'package:agungbudip_my_id/utils/network/network_exceptions.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'portfolio_event.dart';
part 'portfolio_state.dart';

class PortfolioBloc extends Bloc<PortfolioEvent, PortfolioState> {
  final Endpoints endpoints;
  PortfolioBloc(this.endpoints) : super(PortfolioInitial());
  
  List<Portfolio> items = [];
  int currentPage = 1;
  bool hasNext = false;
  
  @override
  Stream<PortfolioState> mapEventToState(
    PortfolioEvent event,
  ) async* {
    if (event is PortfolioOnRefresh) {
      yield PortfolioRefresh();

      Map<String, dynamic> params = Map();
      params['page'] = 1;

      var res = await endpoints.getPortfolios(params: params);

      yield* res.when(
        success: (portfolioRes) async* {
          if(portfolioRes.status){
            items.clear();
            items.addAll(portfolioRes.data);
            var pagination = portfolioRes.meta.pagination;
            currentPage = pagination.currentPage;
            hasNext = pagination.totalPages > currentPage;
            yield PortfolioRefreshSuccess();
          }else{
            yield PortfolioRefreshError(portfolioRes.message);
          }
        },
        failure: (NetworkExceptions error) async* {
          yield PortfolioRefreshError(NetworkExceptions.getErrorMessage(error));
        },
      );
    } else if (event is PortfolioOnLoad) {
      yield PortfolioLoad();

      Map<String, dynamic> params = Map();
      params['page'] = currentPage + 1;

      var res = await endpoints.getPortfolios(params: params);
      yield* res.when(
        success: (portfolioRes) async* {
          if(portfolioRes.status){
            items.addAll(portfolioRes.data);
            var pagination = portfolioRes.meta.pagination;
            currentPage = pagination.currentPage;
            hasNext = pagination.totalPages > currentPage;
            yield PortfolioRefreshSuccess();
          }else{
            yield PortfolioRefreshError(portfolioRes.message);
          }
        },
        failure: (NetworkExceptions error) async* {
          yield PortfolioLoadError(NetworkExceptions.getErrorMessage(error));
        },
      );
    }
  }
}
