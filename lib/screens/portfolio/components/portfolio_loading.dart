import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class PortfolioLoading extends StatelessWidget {
  const PortfolioLoading({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(kDefaultPadding),
      child: Center(
        child: SpinKitDoubleBounce(
          color: kPrimaryColor,
          size: 48,
        ),
      ),
    );
  }
}