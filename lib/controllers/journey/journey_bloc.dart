import 'dart:async';

import 'package:agungbudip_my_id/models/journey.dart';
import 'package:agungbudip_my_id/utils/network/endpoints.dart';
import 'package:agungbudip_my_id/utils/network/network_exceptions.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'journey_event.dart';

part 'journey_state.dart';

class JourneyBloc extends Bloc<JourneyEvent, JourneyState> {
  final Endpoints endpoints;

  JourneyBloc(this.endpoints) : super(JourneyInitial());

  List<Journey> items = [];
  int currentPage = 1;
  bool hasNext = false;

  @override
  Stream<JourneyState> mapEventToState(
    JourneyEvent event,
  ) async* {
    if (event is JourneyOnRefresh) {
      yield JourneyRefresh();

      Map<String, dynamic> params = Map();
      params['page'] = 1;

      var res = await endpoints.getJourneys(params: params);

      yield* res.when(
        success: (journeyRes) async* {
          if(journeyRes.status){
            items.clear();
            items.addAll(journeyRes.data);
            var pagination = journeyRes.meta.pagination;
            currentPage = pagination.currentPage;
            hasNext = pagination.totalPages > currentPage;
            yield JourneyRefreshSuccess();
          }else{
            yield JourneyRefreshError(journeyRes.message);
          }
        },
        failure: (NetworkExceptions error) async* {
          yield JourneyRefreshError(NetworkExceptions.getErrorMessage(error));
        },
      );
    } else if (event is JourneyOnLoad) {
      yield JourneyLoad();

      Map<String, dynamic> params = Map();
      params['page'] = currentPage + 1;

      var res = await endpoints.getJourneys(params: params);
      yield* res.when(
        success: (journeyRes) async* {
          if(journeyRes.status){
            items.addAll(journeyRes.data);
            var pagination = journeyRes.meta.pagination;
            currentPage = pagination.currentPage;
            hasNext = pagination.totalPages > currentPage;
            yield JourneyRefreshSuccess();
          }else{
            yield JourneyRefreshError(journeyRes.message);
          }
        },
        failure: (NetworkExceptions error) async* {
          yield JourneyLoadError(NetworkExceptions.getErrorMessage(error));
        },
      );
    }
  }
}
