import 'package:flutter/material.dart';

//COLOR
const kPrimaryColor = Color(0xFF42489E);
const kAccentColor = Color(0xFFBBB5DB);
const kGreyColor1 = Color(0xFF666666);
const kGreyColor2 = Color(0xFF9B9B9B);
const kBgColor = Color(0xFFE7E7E7);
const kMaterialPrimaryColor = MaterialColor(
  0xFF42489E,
  <int, Color>{
    50: Color(0xFF7e83c9),
    100: Color(0xFF6f75c3),
    200: Color(0xFF6167bd),
    300: Color(0xFF5259b7),
    400: Color(0xFF484fad),
    500: Color(0xFF42489E),
    600: Color(0xFF3c4290),
    700: Color(0xFF363b81),
    800: Color(0xFF303573),
    900: Color(0xFF2a2e65),
  },
);

//GRADIENT
const kGradientColor = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [
    Color(0xFF6D5DA8),
    Color(0xFF43479E),
  ],
);

//VALUE
const kDefaultPadding = 20.0;
const kMaxWidth = 1232.0;
const kConnectionTimeout = 60000;
const kConnectionReadTimeout = 30000;
const kConnectionWriteTimeOut = 30000;

//TEXT STYLE
const kStyleRobotoBold36 = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 36,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoBold24 = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 24,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoNormal24 = TextStyle(
    fontFamily: "Roboto",
    color: Color(0x80FFFFFF),
    fontSize: 24,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoBold24NoHeight = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 24,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.5,
);
const kStyleRobotoNormal24NoHeight = TextStyle(
    fontFamily: "Roboto",
    color: Color(0x80FFFFFF),
    fontSize: 24,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
);
const kStyleRobotoNormal14 = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoNormal14NoHeight = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
);
const kStyleRobotoNormal12 = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 12,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoNormal12NoHeight = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 12,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
);
const kStyleRobotoNormal15 = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 15,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoBold14 = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.5,
    height: 1.7
);
const kStyleRobotoBold14NoHeight = TextStyle(
    fontFamily: "Roboto",
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.5,
);
const kStylePoppinsSemiBold14 = TextStyle(
    fontFamily: "Poppins",
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.33
);
const kStylePoppinsBold14 = TextStyle(
    fontFamily: "Poppins",
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w700,
    letterSpacing: 0.33
);
