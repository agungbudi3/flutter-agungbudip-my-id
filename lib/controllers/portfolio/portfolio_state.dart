part of 'portfolio_bloc.dart';

@immutable
abstract class PortfolioState {}

class PortfolioInitial extends PortfolioState {}

class PortfolioRefresh extends PortfolioState {}

class PortfolioRefreshSuccess extends PortfolioState {}

class PortfolioRefreshError extends PortfolioState {
  final String message;

  PortfolioRefreshError(this.message);
}

class PortfolioLoad extends PortfolioState {}

class PortfolioLoadSuccess extends PortfolioState {}

class PortfolioLoadError extends PortfolioState {
  final String message;

  PortfolioLoadError(this.message);
}
