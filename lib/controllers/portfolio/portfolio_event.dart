part of 'portfolio_bloc.dart';

@immutable
abstract class PortfolioEvent {}

class PortfolioOnRefresh extends PortfolioEvent {}
class PortfolioOnLoad extends PortfolioEvent {}
