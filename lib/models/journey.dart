class Journey {
  int _id;
  String _year;
  String _title;
  String _subtitle;
  String _info;
  String _story;

  int get id => _id;

  String get year => _year;

  String get title => _title;

  String get subtitle => _subtitle;

  String get info => _info;

  String get story => _story;

  Journey(
      {int id,
      String year,
      String title,
      String subtitle,
      String info,
      String story}) {
    _id = id;
    _year = year;
    _title = title;
    _subtitle = subtitle;
    _info = info;
    _story = story;
  }

  Journey.fromJson(dynamic json) {
    _id = json["id"];
    _year = json["year"];
    _title = json["title"];
    _subtitle = json["subtitle"];
    _info = json["info"];
    _story = json["story"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["year"] = _year;
    map["title"] = _title;
    map["subtitle"] = _subtitle;
    map["info"] = _info;
    map["story"] = _story;
    return map;
  }
}