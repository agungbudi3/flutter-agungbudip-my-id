import 'package:agungbudip_my_id/models/journey.dart';
import 'package:agungbudip_my_id/models/remote/meta.dart';

class JourneyRes {
  bool _status;
  String _message;
  List<Journey> _data;
  Meta _meta;

  bool get status => _status;

  String get message => _message;

  List<Journey> get data => _data;

  Meta get meta => _meta;

  JourneyRes({bool status, String message, List<Journey> data, Meta meta}) {
    _status = status;
    _message = message;
    _data = data;
    _meta = meta;
  }

  JourneyRes.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Journey.fromJson(v));
      });
    }
    _meta = json["meta"] != null ? Meta.fromJson(json["meta"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    if (_meta != null) {
      map["meta"] = _meta.toJson();
    }
    return map;
  }
}
