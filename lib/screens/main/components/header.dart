import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 150,
          height: 150,
          child: Container(
            width: 150,
            height: 150,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage("assets/images/avatar.jpg"),
              ),
            ),
          ),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              color: Colors.white,
              width: kDefaultPadding / 4,
            ),
          ),
        ),
        SizedBox(height: kDefaultPadding),
        FittedBox(
          fit: BoxFit.fitWidth,
          child: Text.rich(
            TextSpan(
              text: "AGUNG",
              style: kStyleRobotoBold24,
              children: [
                TextSpan(
                  text: " BUDI",
                  style: kStyleRobotoNormal24.apply(
                    color: Colors.white.withOpacity(0.5),
                  ),
                ),
                TextSpan(
                  text: " PRASETYO",
                  style: kStyleRobotoBold24,
                )
              ],
            ),
            textAlign: TextAlign.center,
          ),
        ),
        FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(
            "SOFTWARE ENGINEER | MOBILE APP DEVELOPER",
            style: kStyleRobotoNormal24.apply(
              color: Colors.white.withOpacity(0.5),
            ),
          ),
        ),
      ],
    );
  }
}
