import 'package:agungbudip_my_id/my_app.dart';
import 'package:agungbudip_my_id/utils/flavor_config.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FlavorConfig(
    flavor: Flavor.DEV,
    values: FlavorValues(
      baseUrl: "http://localhost:8082/",
    ),
  );
  runApp(MyApp());
}
