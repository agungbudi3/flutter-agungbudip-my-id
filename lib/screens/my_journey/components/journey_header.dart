import 'package:agungbudip_my_id/utils/constants.dart';
import 'package:flutter/material.dart';

class JourneyHeader extends StatelessWidget {
  const JourneyHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: kDefaultPadding),
        Text(
          "MY JOURNEY",
          style: kStyleRobotoBold36.apply(
            color: kPrimaryColor,
          ),
        ),
        SizedBox(height: kDefaultPadding / 2),
        Text(
          "See my journey below, I write down from the beginning of my college until my most recent work experience.",
          style: kStyleRobotoNormal14.apply(
            color: kGreyColor1,
          ),
          textAlign: TextAlign.start,
        ),
      ],
    );
  }
}