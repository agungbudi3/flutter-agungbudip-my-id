import 'package:agungbudip_my_id/screens/contact_me/contact_me.dart';
import 'package:agungbudip_my_id/screens/my_journey/my_journey_screen.dart';
import 'package:agungbudip_my_id/screens/portfolio/portfolio.dart';
import 'package:agungbudip_my_id/utils/enums.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class MenuCubit extends Cubit<MenuItemEnum> {
  List<MenuItemEnum> menuItems = [
    MenuItemEnum.MY_JOURNEY,
    MenuItemEnum.PORTFOLIO,
    MenuItemEnum.CONTACT_ME,
  ];

  MenuCubit() : super(MenuItemEnum.MY_JOURNEY);

  openMenu(MenuItemEnum menuItem) => emit(menuItem);

  static String getMenuItemText(MenuItemEnum menuItem) {
    switch (menuItem) {
      case MenuItemEnum.MY_JOURNEY:
        return "MY JOURNEY";
      case MenuItemEnum.PORTFOLIO:
        return "PORTFOLIO";
      case MenuItemEnum.CONTACT_ME:
        return "CONTACT ME";
      default:
        return "";
    }
  }

  static Widget getMenuItemWidget(MenuItemEnum menuItem) {
    switch (menuItem) {
      case MenuItemEnum.MY_JOURNEY:
        return MyJourney();
      case MenuItemEnum.PORTFOLIO:
        return Portfolio();
      case MenuItemEnum.CONTACT_ME:
        return ContactMe();
      default:
        return Container();
    }
  }
}
