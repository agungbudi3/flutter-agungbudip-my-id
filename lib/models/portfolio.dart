class Portfolio {
  int _id;
  String _name;
  String _description;
  List<String> _images;
  List<String> _tags;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  List<String> get images => _images;
  List<String> get tags => _tags;

  Portfolio({
    int id,
    String name,
    String description,
    List<String> images,
    List<String> tags}){
    _id = id;
    _name = name;
    _description = description;
    _images = images;
    _tags = tags;
  }

  Portfolio.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _images = json["images"] != null ? json["images"].cast<String>() : [];
    _tags = json["tags"] != null ? json["tags"].cast<String>() : [];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["images"] = _images;
    map["tags"] = _tags;
    return map;
  }

}